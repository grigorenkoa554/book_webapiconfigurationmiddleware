﻿using Book_WebApiConfigurationMiddleWare.DbContextForMiddleWare;
using Book_WebApiConfigurationMiddleWare.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Book_WebApiConfigurationMiddleWare.Repository
{
    public interface IBaseRepository<T> : IDisposable
    {
        void Add(T some);
        void AddRange(IEnumerable<T> list);
        T GetById(int id);
        List<T> GetAll();
        void Delete(int id);
        void Update(T item);
    }
    public class BaseRepository<T> : IBaseRepository<T>, IDisposable where T : Base, new()
    {
        protected readonly MyDbContext dbContext = new MyDbContext();
        protected readonly DbSet<T> table;
        public BaseRepository()
        {
            table = dbContext.Set<T>();
        }
        public void Add(T some)
        {
            table.Add(some);
            dbContext.SaveChanges();
        }

        public void AddRange(IEnumerable<T> list)
        {
            table.AddRange(list);
            dbContext.SaveChanges();
        }
        public void Delete(int id)
        {
            var item = table.First(c => c.Id == id);
            table.Remove(item);
            dbContext.SaveChanges();
        }
        public void Update(T item)
        {
            table.Update(item);
            dbContext.SaveChanges();
        }
        public void Dispose()
        {
            dbContext?.Dispose();
        }

        public T GetById(int id)
        {
            var item = table.First(y => y.Id == id);
            return item;
        }
        public List<T> GetAll()
        {
            return table.ToList();
        }

    }
}
