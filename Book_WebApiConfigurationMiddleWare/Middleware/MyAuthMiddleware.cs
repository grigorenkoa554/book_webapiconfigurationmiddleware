﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Book_WebApiConfigurationMiddleWare.Middleware
{
    public class MyAuthMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly string tocken;

        public MyAuthMiddleware(RequestDelegate next, string tocken = "12345678")
        {
            this._next = next;
            this.tocken = tocken;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var token = context.Request.Query["token"];
            if (token == tocken)
            {
                await _next.Invoke(context);
            }
            else
            {
                context.Response.StatusCode = 403;
                await context.Response.WriteAsync("Token is invalid");
            }
        }
    }

    public static class TokenExtensions
    {
        public static IApplicationBuilder UseMyAuthToken(this IApplicationBuilder builder, string token)
        {
            return builder.UseMiddleware<MyAuthMiddleware>(token);
        }
    }
}
