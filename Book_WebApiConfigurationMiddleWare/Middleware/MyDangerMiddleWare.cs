﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Book_WebApiConfigurationMiddleWare.Middleware
{
    public class MyDangerMiddleWare
    {
        private readonly RequestDelegate next;
        private readonly string tocken;

        public MyDangerMiddleWare(RequestDelegate n, string tocken = "121212")
        {
            this.next = n;
            this.tocken = tocken;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var token = context.Request.Query["token"];
            if (token == tocken)
            {
                await next.Invoke(context);
            }
            else
            {
                context.Response.StatusCode = 403;
                await context.Response.WriteAsync("invalid tocken");
            }
        }
    }
    public static class Extensions
    {
        public static IApplicationBuilder UseDangerToken(this IApplicationBuilder builder, string token)
        {
            return builder.UseMiddleware<MyDangerMiddleWare>(token);
        }
    }
}
