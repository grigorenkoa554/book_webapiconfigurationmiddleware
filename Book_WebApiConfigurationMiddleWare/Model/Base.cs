﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Book_WebApiConfigurationMiddleWare.Model
{
    public class Base
    {
        public int Id { get; set; }
    }
    [Table("User")]
    public class User : Base
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public Telephone Telephone { get; set; }
    }

    [Table("Telephone")]
    public class Telephone : Base
    {
        public string Number { get; set; }
        public string Company { get; set; }

        public User User { get; set; }
        public int UserId { get; set; }
    }
}

