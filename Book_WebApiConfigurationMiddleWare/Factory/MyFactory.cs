﻿using Book_WebApiConfigurationMiddleWare.Model;
using Book_WebApiConfigurationMiddleWare.Repository;

namespace Book_WebApiConfigurationMiddleWare.Factory
{

    public interface IFactory
    {
        public IBaseRepository<User> GetUserRepository();
        public IBaseRepository<Telephone> GetTelephoneRepository();
    }

    public class MyFactory : IFactory
    {
        public IBaseRepository<Telephone> GetTelephoneRepository()
        {
            return new BaseRepository<Telephone>();
        }

        public IBaseRepository<User> GetUserRepository()
        {
            return new BaseRepository<User>();
        }
    }
}
