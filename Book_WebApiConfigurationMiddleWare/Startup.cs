﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Book_WebApiConfigurationMiddleWare.DbContextForMiddleWare;
using Book_WebApiConfigurationMiddleWare.Factory;
using Book_WebApiConfigurationMiddleWare.Middleware;
using Book_WebApiConfigurationMiddleWare.Model;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;

namespace Book_WebApiConfigurationMiddleWare
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IFactory, MyFactory>();
            services.AddTransient<MyDbContext, MyDbContext>();
            //services.AddScoped<MyDbContext, MyDbContext>();
            //services.AddSingleton<MyDbContext, MyDbContext>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IFactory factory)
        {
            Demo9(app, env, factory);
            //Demo8(app, env);
            //Demo7(app, env);
            //Demo6(app, env);
            //Demo4(app);
            //Demo3(app, env);
            //Demo2(app, env);
            // Demo1(app, env);
            // Demo(app, env);

        }


        public void Demo9(IApplicationBuilder app, IWebHostEnvironment env, IFactory factory)
        {
            app.UseDangerToken("bybyby");

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/by", async context =>
                {
                    var tempClass = new TempClass();
                    var result = tempClass.GetTemp();
                    await context.Response.WriteAsync(result);
                });
                endpoints.MapGet("/byby", async context =>
                {
                    var userController = new UserController();
                    var result = userController.GetUser();
                    await context.Response.WriteAsync(result);
                });
                endpoints.MapGet("/user/all", async context =>
                {
                    var repository = factory.GetUserRepository();
                    var users = repository.GetAll();
                    var str = JsonConvert.SerializeObject(users);
                    await context.Response.WriteAsync(str);
                });
                endpoints.MapPost("/user/add", async context =>
                {
                    var bodyStr = "";
                    var req = context.Request;


                    using (StreamReader reader = new StreamReader(req.Body, Encoding.UTF8, true, 1024, true))
                    {
                        bodyStr = await reader.ReadToEndAsync();
                    }


                    var user = JsonConvert.DeserializeObject<User>(bodyStr);


                    var repository = factory.GetUserRepository();
                    repository.Add(user);
                    var responseBody = JsonConvert.SerializeObject(user);

                    await context.Response.WriteAsync(responseBody);
                });
                endpoints.MapPost("/telephone/add", async context =>
                {
                    var bodyStr = "";
                    var req = context.Request;

                    try
                    {
                        using (StreamReader reader = new StreamReader(req.Body, Encoding.UTF8, true, 1024, true))
                        {
                            bodyStr = await reader.ReadToEndAsync();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }

                    var telephone = JsonConvert.DeserializeObject<Telephone>(bodyStr);


                    var repository = factory.GetTelephoneRepository();
                    repository.Add(telephone);
                    var responseBody = JsonConvert.SerializeObject(telephone);

                    await context.Response.WriteAsync(responseBody);
                });
            });

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Page not found.");
            });
        }

        public void Demo8(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDangerToken("1212");

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("You in mapGet1");
                });
                endpoints.MapGet("/qq", async context =>
                {
                    await context.Response.WriteAsync("You in mapGet2");
                });
            });

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Page not found.");
            });
        }

        public void Demo7(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //app.Use(async (context, next) =>
            //{
            //    var token = context.Request.Query["token"];
            //    if (token == "12345678")
            //    {
            //        await next.Invoke();
            //    }
            //    else
            //    {
            //        context.Response.StatusCode = 403;
            //        await context.Response.WriteAsync("Token is invalid");
            //    }
            //});
            // app.UseMiddleware<MyAuthMiddleware>();
            app.UseMyAuthToken("555");

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
                endpoints.MapGet("/sdf", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
            });

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Page not found.");
            });
        }

        public void Demo6(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
                endpoints.MapGet("/sdf", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
            });
            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Page not found.");
            });
        }

        public void Demo5(IApplicationBuilder app)
        {
            app.MapWhen(context =>
            {

                return context.Request.Query.ContainsKey("id") &&
                        context.Request.Query["id"] == "5";
            }, HandleId);

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Good bye, World...");
            });
        }

        private static void HandleId(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                await context.Response.WriteAsync("id is equal to 5");
            });
        }
        public void Demo4(IApplicationBuilder app)
        {
            //https://localhost:44301/home/about
            app.Map("/home", home =>
            {
                home.Map("/index", Index);
                home.Map("/about", About);
            });

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Page Not Found");
            });
        }
        private static void Index(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                await context.Response.WriteAsync("Index2");
            });
        }
        private static void About(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                await context.Response.WriteAsync("About");
            });
        }
        private void Demo3(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.Use(async (context, next) =>
            {
                if (context.Request.Path.ToString().ToLower() == @"/temp")
                {

                    var tempClass = new TempClass();
                    var res = tempClass.GetTemp();
                    await context.Response.WriteAsync(res);
                }
                else
                {
                    await next.Invoke();
                }
            });
            app.Use(async (context, next) =>
            {
                await context.Response.WriteAsync($"Use2 {Environment.NewLine}");
                if (context.Request.Path.ToString().Contains("/"))
                {
                    await context.Response.WriteAsync($"We are contain / {Environment.NewLine}");
                }
                else
                {
                    await next.Invoke();
                }
            });
            app.Run(async (context) =>
            {
                await context.Response.WriteAsync($"Ohh, we are in Run now {Environment.NewLine}");
            });
        }

        private void Demo2(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var temp = "q";

            app.Use(async (context, next) =>
            {
                if (context.Request.Path.ToString().ToLower() == @"/api/user")
                {
                    var userController = new UserController();
                    var result = userController.GetUser();
                    await context.Response.WriteAsync(result);
                    //await context.Response.WriteAsync($"Я был в юзер контроллере {Environment.NewLine}");
                }
                else
                {
                    await next.Invoke();
                }
            });
            app.Map("/index", Index);
            app.Map("/api/about", About);
            app.Use(async (context, next) =>
            {
                await context.Response.WriteAsync($"Begin {Environment.NewLine}");
                if (context.Request.Path.ToString().Contains("q"))
                {
                    await context.Response.WriteAsync($"We contains q {Environment.NewLine}");
                }
                else
                {
                    await next.Invoke();
                }
                await context.Response.WriteAsync($"End {Environment.NewLine}");
            });
            app.Use(async (context, next) =>
            {
                await context.Response.WriteAsync($"BeforeOne {Environment.NewLine}");
                await next.Invoke();
                await context.Response.WriteAsync($"AfterOne {Environment.NewLine}");
            });
            app.Use(async (context, next) =>
            {
                await context.Response.WriteAsync($"This one! {Environment.NewLine}");
                await next.Invoke();
            });
            app.Run(async (context) =>
            {
                await context.Response.WriteAsync($"This two! {Environment.NewLine}");
            });
        }

        private void Demo1(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment() || env.IsKakugodno()) // env.EnvironmentName == "Kakugodno"
            {
                app.Run(async (context) =>
                {
                    var query = "";
                    if (context.Request.Query.Count > 0)
                    {
                        query = context.Request.Query.Select(x => $"'{x.Key}' - '{x.Value}'").First();
                        //.Aggregate((x, y) => $"{x} || {y}");
                    }
                    await context.Response.WriteAsync(@$"Environment: {env.EnvironmentName}, AppName: {env.ApplicationName}!
                    ContentRootPath: {env.ContentRootPath}, WebRootPath: {env.WebRootPath},
                    Path: {context.Request.Path}, Query: {query}");
                });
            }
            if (env.IsStaging() || env.IsEnvironment("SomeEnv")) // env.EnvironmentName == "Kakugodno"
            {
                app.Run(async (context) =>
                {
                    await context.Response.WriteAsync($"Environment2: {env.EnvironmentName}, AppName2: {env.ApplicationName}!");
                });
            }
            if (env.IsGood())
            {
                app.Run(async (context) =>
                {
                    await context.Response.WriteAsync($"Environment3: {env.EnvironmentName}, AppName3: {env.ApplicationName}!");
                });
            }
            var value = 2;
            app.Use(async (context, next) =>
            {
                if (value > 2000000)
                {
                    value = 2;
                }
                await next.Invoke();
            });
            app.Use(async (context, next) =>
            {
                value = value * 2;
                await context.Response.WriteAsync($"Some Hello world '{value}'!{Environment.NewLine}");
                await next.Invoke();
            });
            app.Run(async (context) =>
            {
                await context.Response.WriteAsync($"Environment: {env.EnvironmentName}, AppName: {env.ApplicationName}!");
            });

            return;
        }

        private void Demo(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
            });
        }
    }
}
