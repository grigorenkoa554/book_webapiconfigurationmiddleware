﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Book_WebApiConfigurationMiddleWare
{
    public static class HostEnvironmentEnvExtensions
    {

        public static bool IsKakugodno(this IHostEnvironment hostEnvironment)
        {
            return hostEnvironment.EnvironmentName == "Kakugodno";
        }

        public static bool IsGood (this IHostEnvironment hostEnvironment)
        {
            return hostEnvironment.EnvironmentName == "Good";
        }

    }
}
