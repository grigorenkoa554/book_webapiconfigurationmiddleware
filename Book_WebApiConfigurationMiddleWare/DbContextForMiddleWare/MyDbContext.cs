﻿using Book_WebApiConfigurationMiddleWare.Model;
using Microsoft.EntityFrameworkCore;

namespace Book_WebApiConfigurationMiddleWare.DbContextForMiddleWare
{
    public class MyDbContext : DbContext
    {
        public MyDbContext()
        {
            Database.EnsureCreated();
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Telephone> Telephones { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var cs = @"Data Source = 'DESKTOP-IOHQV8L'; 
                Initial Catalog='DBForMiddleWare';
                Integrated Security=true;";
            optionsBuilder.UseSqlServer(cs);
        }
    }
}
